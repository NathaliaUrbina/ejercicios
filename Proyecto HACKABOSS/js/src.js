'use strict';
import { getRandomEmoji, desordenar } from './helpers.js';
function createArrayEmojies() {
    //Obtengo 8 emojis random
    let listaEmojisDiferentes = new Array();
    for (let i = 0; i < 8; i++) {
        listaEmojisDiferentes.push(getRandomEmoji());
    }
    //LLeno un array con la lista duplicada
    let listaEmojisFinal = [...listaEmojisDiferentes, ...listaEmojisDiferentes];
    //Desordenamos el array
    listaEmojisFinal = desordenar(listaEmojisFinal);

    return listaEmojisFinal;
}

function crearTablero(emoji) {
    //Creamos un fragmento de codigo
    const frag = document.createDocumentFragment();
    for (let i = 15; i >= 0; i--) {
        //Creamos un li
        const cuadradoLi = document.createElement('li');
        //Creamos un atributo para almacenar clases e index.
        cuadradoLi.setAttribute('class', 'card');
        cuadradoLi.setAttribute('index', i);
        cuadradoLi.innerHTML = `
        <div class="content">
            <div class="back">${emoji[i]}</div>
            <div class="front">❔</div>
        </div>`;
        frag.prepend(cuadradoLi);
    }
    tablero.append(frag);
    const cards = document.querySelectorAll('.card');
    for (const card of cards) {
        card.addEventListener('click', reveal);
    }
}

function refrescarIntentos() {
    intentos++;
    pintarMensaje('intento', `Intentos: ${intentos}`);
}

//Creamos la función que me reinicia los intentos y me gira las cartas
function reiniciarJuego() {
    //Limpio las variables necesarias para que el juego inicie de cero
    emojies = createArrayEmojies();
    intentos = 0;
    contadorAciertos = 0;
    turno = 0;
    previousCard = '';
    //Reinicio los mensajes de intentos y borro cualquier información que se haya mostrado
    pintarMensaje('intento', `Intentos: 0`);
    pintarMensaje('informacion', ``);
    eliminarTodasLasCartasDelTablero();
    //vuelvo a pintar el tablero
    crearTablero(emojies);
}

/**
 * Función que sirve para mostrar un mensaje en un elemento html
 * @param {*} id identificador del elemento html
 * @param {*} mensaje mensaje que se va a mostrar
 */
function pintarMensaje(id, mensaje) {
    document.getElementById(id).innerHTML = mensaje;
}

/**
 * Función que sirve para eliminar todas las cartas del tablero actual
 */
function eliminarTodasLasCartasDelTablero() {
    //Se itera sobre 16 ya que son 16 cartas las que tiene el juego
    for (let i = 0; i < 16; i++) {
        const lastItem = document.querySelector('ul li:last-child');
        tablero.removeChild(lastItem);
    }
}
//Creamos el botón con DOM
function createResetButton() {
    let boton = document.createElement('button');
    boton.innerText = 'Reiniciar';
    //Se asocia la función para reiniciar el juego
    boton.onclick = function () {
        reiniciarJuego();
    };
    let mains = document.querySelector('main');
    mains.append(boton);
}

const reveal = (e) => {
    const currentCard = e.currentTarget;
    //Mostramos una carta
    currentCard.classList.add('flipped');
    //Obtenemos el index de la carta.
    const indexActual = currentCard.getAttribute('index');
    const emojiActual = emojies[indexActual];
    //Primer intento
    pintarMensaje('informacion', ``);
    if (turno === 0) {
        turno++;
        previousCard = currentCard;
    } else if (indexActual !== previousCard.getAttribute('index')) {
        refrescarIntentos();
        turno = 0;
        const indexAnterior = previousCard.getAttribute('index');
        const emojiAnterior = emojies[indexAnterior];
        //Cartas seleccionadas son diferentes
        if (emojiActual !== emojiAnterior) {
            const temporalCard = previousCard;
            pintarMensaje('informacion', `No son iguales.`);
            setTimeout(() => {
                //Comandos para ocultar las cartas
                currentCard.classList.remove('flipped');
                temporalCard.classList.remove('flipped');
                //Limpio el mensaje cuando se ocultan las cartas
                pintarMensaje('informacion', ``);
            }, 1000);
        } else {
            //Comentar
            currentCard.removeEventListener('click', reveal);
            previousCard.removeEventListener('click', reveal);
            pintarMensaje('informacion', `Son iguales.`);
            //Aumento el contador de aciertos para saber cuantos pares se han adivinado
            contadorAciertos++;
            if (contadorAciertos === 8) {
                pintarMensaje(
                    'informacion',
                    `¡Enhorabuena! haz conseguido todos los pares en ${intentos} intentos.`
                );
            }
        }
        previousCard = '';
    } else {
        //CREAR UN AVISO DE SELECCIONANDO LA MISMA.
        pintarMensaje('informacion', `Estas seleccionando la misma carta`);
    }
};

let emojies = createArrayEmojies();
//Creamos tablero
const tablero = document.querySelector('ul.memoria');
crearTablero(emojies);
createResetButton();
let previousCard;
let turno = 0;
let intentos = 0;
let contadorAciertos = 0;
